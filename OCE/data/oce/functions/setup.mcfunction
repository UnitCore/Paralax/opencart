#scoreboards
scoreboard objectives add OCE-ticks dummy {"text":"OCE:ticks","bold":true,"color":"dark_red"}
scoreboard objectives add OCE-second dummy {"text":"OCE:second","bold":true,"color":"dark_red"}

#loading message
tellraw @a ["",{"text":"Loaded","color":"green"},{"text":" ","color":"dark_blue"},{"text":"OpenCartEngine","italic":true,"underlined":true,"color":"dark_blue","clickEvent":{"action":"open_url","value":"https://gitlab.com/UnitCore/Paralax/opencart"}},{"text":"\n"},{"text":"A open-source Project by:","color":"gold"},{"text":"\n"},{"text":"LeMoonStar","color":"dark_aqua"}]
