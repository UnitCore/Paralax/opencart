#one second timer
scoreboard players set OCENGINE OCE-second 0
scoreboard players add OCENGINE OCE-ticks 1
execute if score OCENGINE OCE-ticks matches 20.. run scoreboard players set OCENGINE OCE-second 1
execute if score OCENGINE OCE-ticks matches 20.. run scoreboard players set OCENGINE OCE-ticks 0
execute if score OCENGINE OCE-second matches 1 run function oce:oncepersec

#only temporary to test extension system
function #oce:extensions/whilerace
